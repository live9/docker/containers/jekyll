FROM ruby:2.7.1-buster

WORKDIR /source

# Locales to avoid 'Invalid US-ASCII character "\xE2" on line 10'
RUN apt-get update && \
    apt-get install --no-install-recommends -y locales=2.28-10 && \
    rm -rf /var/lib/apt/lists/* && \
    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

COPY --chown=root:root entrypoint /usr/local/bin/entrypoint
COPY Gemfile .
RUN \
    bundle install && \
    rm -fr Gemfile
