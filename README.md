# Jekyll builder

Theme oriented Jekyll builder, we use tags to do a different container image per theme. Per theme we use the provided `Gemfile` to install all the required gems to build properly that theme.

The default working directory is `/source`. Use Docker volumes to mount your jekyll site there. 

## Tags and themes

* `docs`: [Docs - Responsive Documentation](https://themeforest.net/item/docs-responsive-documentation-manual-jekyll-theme/21131076)
* `personal`: [Personal Jekyll theme](https://jekyllthemes.io/theme/personal-website-jekyll-theme)
* `regcel`: Theme developed by @muygrafico for Karisma's "No más celus vigilados" microsite
